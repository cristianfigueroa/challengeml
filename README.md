# README #

App de ejemplo para el challenge de Mercado libre

### Aplicación mobile realizada en Android ###

* La versión mínima para probar es API 21 - Lollipop 

### Las tecnologías que se usaron para realizar la app: ###

* [Android Navigation](https://developer.android.com/guide/navigation/navigation-getting-started)
* [Android LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [Android ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
* [Retrofit](https://square.github.io/retrofit/)