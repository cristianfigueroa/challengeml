package com.ml.challenge.model

object Search {
    data class Response(val paging: Paging, val results: List<Product>)
    data class Paging(val total: Int, val offset: Int, val limit: Int)
}