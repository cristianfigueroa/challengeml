package com.ml.challenge.model

import com.google.gson.annotations.SerializedName

data class Description(val text: String, @SerializedName("plain_text") val plainText: String)