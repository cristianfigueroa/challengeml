package com.ml.challenge.model

import com.google.gson.annotations.SerializedName

data class Product(
    val id: String, @SerializedName("site_id") val siteId: String,
    val title: String,
    val price: Double, @SerializedName("currency_id") val currencyId: String,
    val thumbnail: String,
    val categoryId: String
)