package com.ml.challenge.io.service

import com.ml.challenge.model.Description
import com.ml.challenge.model.Search
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * ServiceCreator para invocar la api de mercado libre
 *
 */
interface MLService {

    @GET("sites/MLA/search")
    fun search(
        @Query("q") q: String, @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Observable<Search.Response>

    @GET("items/{itemId}/description")
    fun getDescription(@Path("itemId") id: String): Observable<Description>

}