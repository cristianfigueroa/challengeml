package com.ml.challenge.io

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ErrorInterceptor : Interceptor {

    /**
     * Interceptor clase para manejo de errores
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        var response = chain.proceed(request)
        when (response.code()) {
            500 -> throw IOException("Ocurrio un error inesperado, intente mas tarde")
            400 -> throw IOException("Ocurrio un error inesperado, intente mas tarde")
        }

        return response
    }
}