package com.ml.challenge.io.service

import com.ml.challenge.io.ServiceCreator
import com.ml.challenge.model.Description
import com.ml.challenge.model.Search
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MLServiceImpl {

    private val service: MLService = ServiceCreator.Builder().build().create(MLService::class.java)

    /**
     * Búsqueda de productos
     * @param query texto a buscar
     * @param offset valor para recuperar los proximos resultados
     * @param limit limite de resultado
     * @return
     */
    fun search(query: String, offset: Int, limit: Int = 10): Observable<Search.Response> {
        return service.search(query, offset, limit)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Descripción de un producto
     * @param id de Producto
     * @return Observable [Description]
     */
    fun getDescription(id: String): Observable<Description> {
        return service.getDescription(id)
    }
}