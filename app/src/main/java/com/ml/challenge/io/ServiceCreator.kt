package com.ml.challenge.io

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class ServiceCreator(private var endpoint: String) {

    data class Builder(var endpoint: String = "https://api.mercadolibre.com/") {
        fun endpoint(endpoint: String) = apply { this.endpoint = endpoint }
        fun build() = ServiceCreator(endpoint)

    }

    private var logging = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private var httpClient = OkHttpClient.Builder()
        .addInterceptor(logging)
        .addInterceptor(ErrorInterceptor())

    private val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(
            RxJava2CallAdapterFactory.create()
        )
        .addConverterFactory(
            GsonConverterFactory.create()
        )
        .client(httpClient.build())
        .baseUrl(endpoint)
        .build()

    /**
     * Esta función sirve para crear un servicio
     *
     * @param serviceClass Interfaz donde se define todos los servicios
     * @return Service de [T]
     */
    fun <T> create(serviceClass: Class<T>): T {
        return retrofit.create(serviceClass)
    }

}