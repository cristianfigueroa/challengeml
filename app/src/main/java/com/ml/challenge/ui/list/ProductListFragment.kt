package com.ml.challenge.ui.main.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.ml.challenge.R
import com.ml.challenge.model.Product
import com.ml.challenge.ui.BaseFragment
import com.ml.challenge.ui.list.OnProductAdapterListener
import com.ml.challenge.ui.list.ProductAdapter
import com.ml.challenge.ui.viewmodel.ProductListViewModel
import com.ml.challenge.ui.viewmodel.ProductShareViewModel
import kotlinx.android.synthetic.main.product_list_fragment.*

class ProductListFragment : BaseFragment(), OnProductAdapterListener {


    private val onItemClickListener: OnProductAdapterListener = this
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var mAdapter: ProductAdapter

    private lateinit var productListViewModel: ProductListViewModel
    private lateinit var productShareViewModel: ProductShareViewModel


    private val args: ProductListFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.product_list_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mLayoutManager = LinearLayoutManager(context)
        mAdapter = ProductAdapter(onItemClickListener)
        hideKeyboard()
        recyclerView.apply {
            layoutManager = mLayoutManager
            adapter = mAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initViewModel()

        productListViewModel.emptyResult.observe(this, Observer { isEmpty ->
            if (isEmpty) {
                emptyView.visibility = View.VISIBLE
                loading.visibility = View.GONE
            }
        })

        productListViewModel.products.observe(this, Observer { products ->
            mAdapter.data = products
            recyclerView.post {
                mAdapter.notifyDataSetChanged()
                recyclerView.visibility = View.VISIBLE
                loading.visibility = View.GONE
            }
        })

        productListViewModel.isMoreItems.observe(this, Observer { value ->
            mAdapter.isMoreItems = value
            mAdapter.notifyDataSetChanged()
        })

        productListViewModel.error.observe(this, Observer { error ->
            showErrorMessage(error, R.id.mainFragment)
        })

        productListViewModel.setQuery(args.textSearch)
        productListViewModel.fetchProducts()
    }


    private fun initViewModel() {
        productShareViewModel = activity!!.run {
            ViewModelProviders.of(this)[ProductShareViewModel::class.java]
        }

        productListViewModel = ViewModelProviders.of(this)
            .get(ProductListViewModel::class.java)
    }


    override fun onItemClick(product: Product) {
        productShareViewModel.selectedProduct(product)
        findNavController().navigate(R.id.productDetailFragment)
    }

    override fun onLoadMoreItems() {
        productListViewModel.fetchProducts()
    }


}