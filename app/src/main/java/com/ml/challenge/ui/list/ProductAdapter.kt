package com.ml.challenge.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ml.challenge.R
import com.ml.challenge.model.Product
import com.ml.challenge.utils.Format

/**
 * Interfaz para manejo el evento onClick
 *
 */
interface OnProductAdapterListener {

    /**
     * Indica que el usuario presiono sobre un item
     *
     * @param product
     */
    fun onItemClick(product: Product)

    /**
     * Se invoca cuando se llego al final de la lista
     */
    fun onLoadMoreItems()

}

/**
 *
 * Adapter
 * @property data lista de productos a mostrar
 * @property OnProductAdapterListener interface para manejar el evento onClick dentro de un item
 */
class ProductAdapter(
    private val onProductAdapterListener: OnProductAdapterListener
) : RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {

    var data = listOf<Product>()
    var isMoreItems = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val product: Product = data[position]
        var loadingVisibility = View.GONE
        if (isMoreItems && itemCount == position + 1) {
            loadingVisibility = View.VISIBLE
            onProductAdapterListener.onLoadMoreItems()
        }
        holder.bind(product, loadingVisibility)
    }


    inner class MyViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        private var itemTitle: TextView = mView.findViewById(R.id.itemTitle)
        private var itemAmount: TextView = mView.findViewById(R.id.itemAmount)
        private var thumbnail: ImageView = mView.findViewById(R.id.thumbnail)
        private var progressBar: ProgressBar = mView.findViewById(R.id.loading)

        fun bind(product: Product, loadingVisibility: Int) {
            itemTitle.text = product.title
            itemAmount.text = Format.currency(product.price, product.currencyId)
            progressBar.visibility = loadingVisibility
            Glide.with(thumbnail)
                .load(product.thumbnail)
                .placeholder(R.drawable.ic_image)
                .into(thumbnail)
            mView.apply {
                tag = product
                setOnClickListener { v -> onProductAdapterListener.onItemClick(v.tag as Product) }
            }
        }
    }


}