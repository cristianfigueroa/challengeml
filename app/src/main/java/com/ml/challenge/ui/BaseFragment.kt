package com.ml.challenge.ui

import android.content.Context
import android.content.DialogInterface
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.ml.challenge.R

open class BaseFragment : Fragment() {


    /**
     * Muestra un dialogo de error y cuando usuario presiona aceptar vuelve a la pantalla anterior
     * que se indica en el campo [destinationId]
     *
     * @param message Mensaje de error
     * @param destinationId id del fragment que se indica en el xml nav_graph
     */
    fun showErrorMessage(message: String, destinationId: Int) {
        val builder: AlertDialog.Builder = activity!!.let {
            AlertDialog.Builder(it)
        }
        builder.apply {
            setMessage(message)
            setTitle(R.string.error)
            setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()
                findNavController().popBackStack(destinationId, false)
            })
        }.create().show()
    }

    /**
     * Oculta el teclado en una vista
     *
     */
    fun hideKeyboard() {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }
}