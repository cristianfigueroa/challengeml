package com.ml.challenge.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.ml.challenge.R
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchBtn.setOnClickListener { view -> search(view) }
    }

    private fun search(view: View) {
        var query = textSearch.text.toString()
        if (query.isEmpty()) {
            textSearch.error = getString(R.string.text_empty)
        } else {
            var bundle = bundleOf("textSearch" to query)
            view.findNavController().navigate(R.id.productListFragment, bundle)
        }
    }

}
