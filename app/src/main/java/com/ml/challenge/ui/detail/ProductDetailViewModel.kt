package com.ml.challenge.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ml.challenge.io.service.MLServiceImpl
import io.reactivex.schedulers.Schedulers

class ProductDetailViewModel : ViewModel() {

    private val description: MutableLiveData<String> = MutableLiveData()

    fun getDescription(id: String): LiveData<String> {
        fetchDescription(id)
        return description
    }

    private fun fetchDescription(id: String) {
        MLServiceImpl().getDescription(id)
            .subscribeOn(Schedulers.io())
            .subscribe({ response ->
                description.postValue(response.plainText)
            }, { error ->
                description.postValue(error.message)
            })
    }
}