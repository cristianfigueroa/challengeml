package com.ml.challenge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ml.challenge.io.service.MLServiceImpl
import com.ml.challenge.model.Product
import com.ml.challenge.utils.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProductListViewModel : ViewModel() {

    val isMoreItems: MutableLiveData<Boolean> = MutableLiveData()
    val products: MutableLiveData<List<Product>> = MutableLiveData()
    val error: MutableLiveData<String> = MutableLiveData()
    val emptyResult: MutableLiveData<Boolean> = MutableLiveData()

    private var textSearch: String = ""
    private var offset: Int = 0

    fun setQuery(query: String) {
        textSearch = query
        offset = 0
    }

    fun fetchProducts() {
        MLServiceImpl().search(textSearch, offset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                if (response.results.isNotEmpty()) {
                    if (offset == 0) {
                        products.value = response.results
                    } else {
                        var list = products.value!!.toMutableList()
                        list.addAll(response.results)
                        products.value = list
                    }
                    offset += 10
                } else {
                    emptyResult.value = offset == 0
                    isMoreItems.value = false
                }
            }, { e ->
                Logger.error("ProductListViewModel", e.toString(), e.fillInStackTrace())
                error.postValue(e.message)
            })
    }
}