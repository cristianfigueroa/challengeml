package com.ml.challenge.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.ml.challenge.R
import com.ml.challenge.ui.viewmodel.ProductShareViewModel
import com.ml.challenge.utils.Format
import kotlinx.android.synthetic.main.product_detail_fragment.*

class ProductDetailFragment : Fragment() {

    private lateinit var productShareViewModel: ProductShareViewModel
    private lateinit var productDetailViewModel: ProductDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViewModel()

        productShareViewModel.product.observe(this, Observer { product ->
            productTitle.text = product.title
            amountTextView.text = Format.currency(product.price, product.currencyId)
            Glide.with(this)
                .load(product.thumbnail)
                .into(thumbnail)
        })
        productDetailViewModel.getDescription(productShareViewModel.product.value!!.id)
            .observe(this, Observer { description ->
                loading.visibility = View.GONE
                descriptionTextView.visibility = View.VISIBLE
                descriptionTextView.text = description
            })

    }

    private fun initViewModel() {
        productShareViewModel = activity?.run {
            ViewModelProviders.of(this)[ProductShareViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        productDetailViewModel = ViewModelProviders.of(this)
            .get(ProductDetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.product_detail_fragment, container, false)
    }


}