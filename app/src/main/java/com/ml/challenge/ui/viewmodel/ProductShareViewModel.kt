package com.ml.challenge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ml.challenge.model.Product

class ProductShareViewModel : ViewModel() {

    val product: MutableLiveData<Product> = MutableLiveData()

    fun selectedProduct(item: Product) {
        product.postValue(item)
    }


}