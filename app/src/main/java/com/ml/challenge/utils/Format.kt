package com.ml.challenge.utils

import java.text.NumberFormat
import java.util.*

class Format {

    private enum class CurrencyLocale(val value: String, val locale: Locale) {
        AR("ARS", Locale("es", "AR"));

        companion object {
            fun getLocale(iso: String): CurrencyLocale? {
                return CurrencyLocale.values()
                    .find { currencyLocale -> currencyLocale.value == iso }
            }
        }
    }

    companion object {
        /**
         * Función para formatear un valor a moneda
         *
         * @param value monto a formatear
         * @param iso Valor iso. Ej: ARS
         * @return Un string formateado en el valor iso
         */
        fun currency(value: Double, iso: String): String {
            return NumberFormat.getCurrencyInstance(CurrencyLocale.getLocale(iso)!!.locale)
                .format(value)
        }
    }
}