package com.ml.challenge.utils

import android.util.Log
import com.ml.challenge.BuildConfig

class Logger {
    companion object {
        private val isDebug = BuildConfig.DEBUG

        fun log(tag: String, message: String) {
            if (isDebug) {
                Log.i(tag, message)
            } else {
                // Aca se tiene que agregar la logica para manejar los logs en producción
            }
        }

        fun error(tag: String, message: String, throwable: Throwable) {
            if (isDebug) {
                Log.e(tag, message, throwable)
            } else {
                // Aca puede ir el servidor para loguear los errores de la app
            }
        }
    }
}